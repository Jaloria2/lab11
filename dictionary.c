#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid 
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}

    int arrLength = 100, count = 0;
    char **str;
    str = (char **)malloc(arrLength * sizeof(char));
    
	char line[50];

    while(fgets(line, 100, in) != NULL){
        char *nl = strchr(line, '\n');
        if (nl) * nl = '\0';
        if (count == arrLength){
            arrLength += 100;
            *str = (char*)realloc(str, arrLength * sizeof(char));
        }
        
        //strcpy(str[count], line); this doesnt work, it causes "make check" to crash
        
        str[count] = strdup(line);
        //printf("START %s Finish \n" , str[count]);
        printf("START %s Finish line: %i \n" , str[count], count);
        count++;
        
    }
    
    
	// Allocate memory for an array of strings (arr).
	// Read the dictionary line by line.
	// Expand array if necessary (realloc).
	// Allocate memory for the string (str).
	// Copy each line into the string (use strcpy).
	// Attach the string to the large array (assignment =).
	free(str);
	// The size should be the number of entries in the array.
	*size = count;
    fclose(in);
	// Return pointer to the array of strings.
    
	return str;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    
    
    if (dictionary == NULL)  return NULL;
    
	for (int i = 0; i < size; i++)
	{
        
	    if (strcmp(target, dictionary[i]) == 0)
	    {
            
	        return dictionary[i];
	    }
	}
    
	return NULL;
}